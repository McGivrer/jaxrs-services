/**
 * 
 */
package com.serphydose.application.tests.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.serphydose.application.model.Game;
import com.serphydose.application.services.GameService;
import com.serphydose.application.tests.GenericEntityTest;

/**
 * @author 212391884
 * 
 */
public class GameServiceTest extends GenericEntityTest<Game> {

	private static GameService gs;
	
	private static Map<String,Game> games = new HashMap<String,Game>();

	public GameServiceTest() {
		super();
		gs = new GameService(emf);
	}
	
	@Before
	public void setup(){
		games.put("game1", new Game(null,"game1","description game 1"));
		games.put("game2", new Game(null,"game2","description game 2"));
		games.put("game3", new Game(null,"game3","description game 3"));
		createGames();
	}
	

	/**
	 * Test method for
	 * {@link com.serphydose.application.services.DaoService#getAll()}.
	 */
	@Test
	public void testGetAll() {
		List<Game> games = gs.getAll();
		assertEquals("List of games is not coherent", 3, games.size());
	}

	/**
	 * Test method for
	 * {@link com.serphydose.application.services.DaoService#find(java.lang.Object)}
	 * .
	 */
	@Test
	public void testFind() {
		Game justReadedGame = gs.find(new Long(1));
		assertTrue("Game is not saved",justReadedGame!=null);
		assertEquals("Game title is not the right one","game2",justReadedGame.getTitle());
	}
	/**
	 * Test method for
	 * {@link com.serphydose.application.services.DaoService#save(java.lang.Object)}
	 * .
	 */
	@Test
	public void testSave() {
		gs.save(games.get("game1"));
		Game justInsertedGame = gs.find(new Long(1));
		assertTrue("Game is not saved",justInsertedGame!=null);
	}

	/**
	 * Test method for
	 * {@link com.serphydose.application.services.DaoService#update(java.lang.Object)}
	 * .
	 */
	@Test
	public void testUpdate() {
		Game game1 = gs.find(new Long(1));
		game1.setTitle("game1.1");
		gs.update(game1);
		
		Game game11 = gs.find(new Long(1));
		
		assertEquals("Modified title is not 'game1.1'","game1.1",game11.getTitle());
	}

	/**
	 * Test method for
	 * {@link com.serphydose.application.services.DaoService#delete(java.lang.Object)}
	 * .
	 */
	@Test
	public void testDelete() {
		gs.delete(games.get("game1"));
		List<Game> deletedGames = gs.getAll();
		assertEquals("Game 'game1' has not been deleted !",2,deletedGames.size());
	}

	/**
	 * 
	 */
	private void createGames() {
		for(Game  game : games.values()){
			gs.save(game);
		}
	}
	
}
