package com.serphydose.application.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Game{
	
	@Id
	@GeneratedValue
	private Long id;

	private String title;

	private String description;

	/**
	 * Default constructor.
	 */
	public Game(){
		super();
	}

	/**
	 * parameterized Constructor to intialize object at construction time.
	 * @param id
	 * @param title
	 * @param descirption
	 */ 
	public Game(Long id, String title, String description){
		this();
		this.id = id;
		this.title = title;
		this.description = description;
	}

	/**
	 * Getters & Setters
	 */
	public long getId(){
		return id;
	}
	public String getTitle(){
		return title;
	}
	public String getDescription(){
		return description;
	}
	public void setId(long id){
		this.id = id;
	}
	public void setTitle(String title){
		this.title=title;
	}
	public void setDescription(String description){
		this.description=description;
	}

	/**
	 * format Entity to String to debug purpose
	 */
	public String toString(){
		return "Game["
				+ "id:"+id
				+ "title: '"+title+"'"
				+ "description: '"+description+"'"
				+"]";
	} 
}