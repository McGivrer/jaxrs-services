package com.serphydose.application.services;


import javax.persistence.EntityManagerFactory;

import com.serphydose.application.model.Game;


public class GameService extends DaoService<Game,Long>{ 

	public GameService(){
		super();
	}

	public GameService(EntityManagerFactory emf){
		super(emf);
	}
}