package com.serphydose.application.services;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DaoService<T, PK> {

	Logger logger = LoggerFactory.getLogger(DaoService.class);

	EntityManagerFactory entityManagerFactory;
	EntityManager entityManager;

	/**
	 * Entity Class name manipulated by this DAO.
	 */
	protected Class<T> entityClass;

	/**
	 * Default Constructor.
	 */
	public DaoService() {
		ParameterizedType genericSuperClass = (ParameterizedType) getClass()
				.getGenericSuperclass();
		@SuppressWarnings("unchecked")
		Class<T> entityClass = (Class<T>) genericSuperClass
				.getActualTypeArguments()[0];
		this.entityClass = entityClass;
	}

	/**
	 * Intialize DAO with an new EM Factory.
	 * 
	 * @param entitymanegerFactiry
	 *            The EntityManaegrFactory to used to instanciate EntityManager
	 *            and entities.
	 */
	public DaoService(EntityManagerFactory entityManagerFactory) {
		this();
		logger.debug("Initialize EntityManagerFactory with "
				+ entityManagerFactory);
		this.entityManagerFactory = entityManagerFactory;
		this.entityManager = entityManagerFactory.createEntityManager();
		logger.debug("Create an entityManager: " + entityManager);
	}

	/**
	 * Retrieve all entities for class of T.
	 */
	@SuppressWarnings("unchecked")
	public List<T> getAll() {
		logger.debug("Retrieve entity " + entityClass);
		Query q = entityManager.createQuery("SELECT x from " + entityClass.getCanonicalName() +  " x");
		return (List<T>) q.getResultList();
	}

	/**
	 * retrieve the entity for T class with PK id
	 * 
	 * @param id
	 *            Unique identifier for this entity to be retrieve.
	 */
	public T find(PK id) {
		logger.debug("Retrieve entity " + entityClass + " for id " + id);
		return entityManager.find(entityClass, id);
	}

	/**
	 * Persist the entity into entityManager.
	 * 
	 * @param entity
	 *            the entity to add to persistence engine.
	 */
	public T save(T entity) {
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();
		return entity;
	}

	/**
	 * update an already existing entity into entityManager.
	 * 
	 * @param entity
	 *            the entity to update in persistence engine.
	 */
	public T update(T entity) {
		entityManager.merge(entity);
		return entity;
	}

	/**
	 * delete an already existing entity into entityManager.
	 * 
	 * @param entity
	 *            the entity to delete from persistence engine.
	 */
	public void delete(T entity) {
		entityManager.getTransaction().begin();
		entityManager.remove(entity);
		entityManager.getTransaction().commit();
	}
}