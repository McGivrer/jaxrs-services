package com.serphydose.application.rest;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("/rest")
public class ApplicationService extends Application {
	
	private EntityManagerFactory entityManagerFactory= Persistence.createEntityManagerFactory("games-rs");;

	@Override
	public Set<Object> getSingletons() {
		Set<Object> singletons = new HashSet<Object>();
		if(entityManagerFactory != null){
			// Add rest service to serve by application
			GameRestService gameRestService = new GameRestService(entityManagerFactory);
			singletons.add(gameRestService);
		}
		return singletons;
	}
}