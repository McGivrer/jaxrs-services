/**
 * 
 */
package com.serphydose.application.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.persistence.EntityManagerFactory;

import com.serphydose.application.model.Game;
import com.serphydose.application.services.GameService;


/**
 * A sample REST Services implementation.
 * 
 * @author FDELORME
 * 
 */
@Path("/games")
public class GameRestService {

	/**
	 * Dao service on Game entity.
	 */
	private GameService grs;

	/**
	 * Default constructor.
	 */
	public GameRestService(){

	}

	/**
	 * Parameterized Constructor to initialize EntityManagerFactory.
	 * @param emf
	 */
	public GameRestService(EntityManagerFactory emf){
		this();
		grs = new GameService(emf);
	}

	/**
	 * Serve the List of <code>Game</code> entity as JSON structure.
	 */
	@Path("/")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		List<Game> games = grs.getAll();
		return Response
				.ok(games.toString())
				.build();
	}

	/**
	 * Serve the <code>Game</code> entity as JSON structure corresponding to the <code>gameId</code>.
	 * @param gameId
	 *        provide the Game corresponding to the gameId unique identifier.
	 */
	@Path("/{gameId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response find(@PathParam("gameId") long gameId) {
		Game game = grs.find(gameId);
		return Response
				.ok(game)
				.build();
	}
}
